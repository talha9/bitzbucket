import Layout from '../components/Layout';

const About = () => (
    <Layout>
        <div>
            <h1>About Page</h1>
            <p>BitzPrice App for showing Bitcoin prices of different countries</p>
        </div>
    </Layout>
);


export default About;